<?php

define('PAYWALLEASY_SETTINGS_URL_ROOT', 'admin/config/system/paywalleasy');
define('PAYWALLEASY_CONTENT_TYPE_MANAGE_PARENT', 'admin/structure/types/manage');

define('PAYWALLEASY_NODE_DEFAULT', 0);
define('PAYWALLEASY_NODE_ENABLED', 1);
define('PAYWALLEASY_NODE_DISABLED', 2);

/**
 * Implements hook_permission()
 */
function paywalleasy_permission() {
  return array(
    'bypass paywall easy' => array(
      'title' => t('Bypass Paywall Easy paywall'),
    ),
  );
}

function paywalleasy_in_test_area() {
  static $inside = NULL;
  if (is_null($inside)) {
    $inside = strpos($_GET['q'], PAYWALLEASY_SETTINGS_URL_ROOT . '/test') !== FALSE;
  }
  return $inside;
}

function paywalleasy_is_active() {
  // Always active on test area
  return variable_get('paywalleasy_site_id', FALSE) && (variable_get('paywalleasy_enable', 0) || paywalleasy_in_test_area());
}

/**
 * Implements hook_init()
 */
function paywalleasy_init() {
  drupal_add_css(drupal_get_path('module', 'paywalleasy') . '/paywalleasy.css');
}

/**
 * Implements hook_menu()
 */
function paywalleasy_menu() {
  $items = array();
  $items['paywalleasy/autocomplete'] = array(
    'title' => 'Paywalleasy node autocomplete',
    'page callback' => 'paywalleasy_node_autocomplete',
    'access arguments' => array('administer site configuration'),    
    'type' => MENU_CALLBACK,
  );

  $items[PAYWALLEASY_SETTINGS_URL_ROOT] = array(
    'title' => 'Paywall Easy',
    'description' => 'Configure Paywall Easy integration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('paywalleasy_settings_form'),
    'access arguments' => array('administer site configuration'),    
    'file' => 'paywalleasy.admin.inc',
  );
  $items[PAYWALLEASY_SETTINGS_URL_ROOT . '/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items[PAYWALLEASY_SETTINGS_URL_ROOT . '/test'] = array(
    'title' => 'Test area',
    'page callback' => 'paywalleasy_test_area_page',
    'access arguments' => array('administer site configuration'),    
    'type' => MENU_LOCAL_TASK,
    'file' => 'paywalleasy.admin.inc',
  );
  return $items;
}

function paywalleasy_node_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('node')->fields('node', array('title'))->condition('title', '%' . db_like($string) . '%', 'LIKE')->orderBy('title')->range(0, 10)->execute();
    foreach ($result as $record) {
      $matches[$record->title] = check_plain($record->title);
    }
  }

  drupal_json_output($matches);
}

/***********************************************************
*                     NODE AUGMENTATION                    *
************************************************************/

function paywalleasy_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  $form['workflow']['node_options']['#options']['paywalleasy'] = t('Enable paywall');
}

function paywalleasy_form_node_form_alter(&$form, &$form_state, $form_id) {
  $node = $form['#node'];
  $node_options = variable_get('node_options_' . $node->type, array('status', 'promote'));
  
  $form['options']['paywalleasy'] = array(
    '#type' => 'radios',
    '#title' => t('Paywall'),
    '#options' => array(
      PAYWALLEASY_NODE_DEFAULT => t('Paywall @default (!link)', array('@default' => in_array('paywalleasy', $node_options) ? t('enabled') : t('disabled'), '!link' => l(t('default'), PAYWALLEASY_CONTENT_TYPE_MANAGE_PARENT . '/' . $node->type, array('attributes' => array('target' => '_blank'))))),
      PAYWALLEASY_NODE_ENABLED => t('Paywall enabled (override)'),
      PAYWALLEASY_NODE_DISABLED => t('Paywall disabled (override)'),
    ),
    '#default_value' => !empty($node->paywalleasy) ? $node->paywalleasy : PAYWALLEASY_NODE_DEFAULT,  
  );
}

function paywalleasy_node_insert($node) {
  paywalleasy_node_update($node);
}

function paywalleasy_node_update($node) {
  if (!empty($node->paywalleasy)) {
    db_merge('node_paywalleasy')->key(array('nid' => $node->nid))->fields(array('paywalleasy' => $node->paywalleasy))->execute();
  }
  else {
    // Don't save to db for default value
    paywalleasy_node_delete($node);
  }
}

function paywalleasy_node_load($nodes) {
  $result = db_select('node_paywalleasy')->fields('node_paywalleasy')->condition('nid', array_keys($nodes))->execute();
  foreach ($result as $record) {
    $nodes[$record->nid]->paywalleasy = $record->paywalleasy;
  }
}

function paywalleasy_node_delete($node) {
  db_delete('node_paywalleasy')->condition('nid', $node->nid)->execute();
}

function paywalleasy_node_paywall_is_enabled($node) {
  // Allow other modules to specify custom logic
  // Implement a hook_paywall_is_enabled() that returns PAYWALLEASY_NODE_DISABLED or PAYWALLEASY_NODE_ENABLED
  $flags = module_invoke_all('paywall_is_enabled', $node);
  if (in_array(PAYWALLEASY_NODE_DISABLED, $flags, TRUE)) {
    return FALSE;
  }
  else if (in_array(PAYWALLEASY_NODE_ENABLED, $flags, TRUE)) {
    return TRUE;
  }

  // Fall through to default logic
  if (!empty($node->paywalleasy)) {
    return $node->paywalleasy == PAYWALLEASY_NODE_ENABLED;
  }
  else {
    // PAYWALLEASY_NODE_DEFAULT
    $node_options = variable_get('node_options_' . $node->type, array('status', 'promote'));
    return in_array('paywalleasy', $node_options);
  }
}

/***********************************************************
*                  PAYWALL IMPLEMENTATION                  *
************************************************************/

/**
 * Implements hook_field_attach_view_alter()
 */
function paywalleasy_field_attach_view_alter(&$output, $context) {
  if (paywalleasy_is_active() && $context['entity_type'] == 'node' && $context['view_mode'] == 'full' && isset($output['body'])) {
    $node = $context['entity'];
    // Users with these three permissions bypass the paywall
    $bypass = user_access('administer nodes') || user_access('administer site configuration') || user_access('bypass paywall easy');

    if (paywalleasy_node_paywall_is_enabled($node) && (!$bypass || paywalleasy_in_test_area())) {
      $instance = field_info_instance('node', 'body', $node->type);    
      $display = field_get_display($instance, 'teaser', $node);

      foreach (element_children($output['body']['#items']) as $index) {
        $item = $output['body']['#items'][$index];

        // Apply text summary to the (already sanitised) body
        $teaser_text = text_summary($output['body'][$index]['#markup'], $instance['settings']['text_processing'] ? $item['format'] : NULL, $display['settings']['trim_length']);

        // Sanity check: Ensure the teaser text is at the beginning of body (in case malformed html brackets got lost or something)
        if (strpos($output['body'][$index]['#markup'], $teaser_text) === 0) {
          // Inject css wrapper for paywall
          $output['body'][$index]['#markup'] = substr_replace($output['body'][$index]['#markup'], '<div class="pw-premium">', strlen($teaser_text), 0) .'</div>';
        }
      }
    }
  }
}

function paywalleasy_preprocess_page(&$vars) {
  if (paywalleasy_is_active()) {
    $text = array();
    $text[] = 'var _pwq = _pwq || [];';
    $text[] = "_pwq.push(['setSiteId', '" . variable_get('paywalleasy_site_id', FALSE) . "']);";
    $text[] = "
(function() {
var pw = document.createElement('script'); pw.type = 'text/javascript'; pw.async = true;
pw.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.paywalleasy.com/v1/embed.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pw, s);
})();";
    drupal_add_js(join("\n", $text), 'inline');
  }
}
