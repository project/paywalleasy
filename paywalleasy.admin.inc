<?php

function paywalleasy_settings_form() {
  $form = array();  
  $form['paywalleasy_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Paywall Easy site id'),
    '#size' => 6,
    '#description' => t('The site id of this site as registered at <a href="http://www.paywalleasy.com">Paywall Easy</a>. This is required for Paywall Easy to work.'),
    '#default_value' => variable_get('paywalleasy_site_id', FALSE),
  );

  $substitions = array('@test-page-url' => url(PAYWALLEASY_SETTINGS_URL_ROOT . '/test'));
  $form['paywalleasy_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Paywall Easy'),
    '#options' => array(
      0 => t('on the <a href="@test-page-url">test area</a> only.', $substitions), 
      1 => t('on entire site.'),    
    ),
    '#description' => t('It is useful to check your settings on the <a href="@test-page-url">test area</a> before enabling Paywall East for the entire site.', $substitions),
    '#default_value' => variable_get('paywalleasy_enable', 0),
  );
  return system_settings_form($form);
}

function paywalleasy_test_area_page($nid = NULL) {
  $messages = array();
  $messages[] = t('This is a test area for testing your Paywall Easy <a href="@url">settings</a>.', array('@url' => url(PAYWALLEASY_SETTINGS_URL_ROOT)));
  $messages[] = t('Click <a !attributes>this link</a> to open the Paywall Easy pop-up.<br />(If a pop-up does not occur, your site id may be incorrect.)', array('!attributes' => 'href="#pw=home" title="' . t('Click to open the Paywall Easy pop-up.') . '"'));
  $messages[] = t('You can view nodes below to observe the Paywall Easy paywall in action.<br />You may need to view several nodes or reload your browser multiple times in order to exceed metered access and trigger the paywall.');

  $build = array();
  $build['messages'] = array(
    '#markup' => '<p>' . join('</p><p>', $messages) . '</p>'
  );
  $build['form'] = drupal_get_form('paywalleasy_test_area_form', $nid);
  return $build;
}

function paywalleasy_test_area_form($form, &$form_state, $nid = NULL) {
  $node = node_load($nid);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t('Enter the title of a node to view it and see how it behaves when Paywall Easy is enabled.'),
    '#autocomplete_path' => 'paywalleasy/autocomplete',
    '#default_value' => !empty($node->nid) ? $node->title : '',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('View node'),
  );

  if (!empty($node->nid)) {
    if (node_access('view', $node)) {
      $form['preview'] = node_view($node);
    }
    else {
      $form['preview'] = array(
        '#markup' => '<p>' . t('You don\'t have sufficient privileges to view this node.') . '</p>'
      );
    }
  }
  return $form;
}

function paywalleasy_test_area_form_validate($form, &$form_state) {
  $nid = db_select('node')->fields('node', array('nid'))->condition('title', $form_state['values']['title'])->range(0, 1)->execute()->fetchField();
  if ($nid) {
    $node = node_load($nid);
    if (!empty($node->nid)) {
      $form_state['nid'] = $nid;
    }
  }
  
  if (empty($form_state['nid'])) {
    form_set_error('title', t('Node with title %title not found.', array('%title' => $form_state['values']['title'])));
  }
}

function paywalleasy_test_area_form_submit($form, &$form_state) {
  // Append the node nid (if exists) to the url arguments and redirect
  if (!empty($form_state['nid'])) {
    $form_state['redirect'] = PAYWALLEASY_SETTINGS_URL_ROOT . '/test/' . $form_state['nid'];
  }
}
